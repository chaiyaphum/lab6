<?php
$doc = new domDocument();
$url = $_GET['url'];
$elemname = $_GET['elemname'];
$keyword = $_GET['keyword'];
$doc->load($url);
$students = $doc->documentElement;
$courses = $students->getElementsByTagName($elemname);
$numCourses = $courses->length;
echo "<html><body><table border='1'><tr><th>Name</th><th>Course</th></tr>";
$pos = false;
for ($i = 0; $i < $numCourses; $i++) {
	$course = $courses->item($i);
	$value = $course->nodeValue;
	$pos = strpos($value, $keyword);
	if ($pos !== false) {
		$student = $course->parentNode->parentNode;
		if ($student instanceof domElement) {
			$names = $student->getElementsByTagName("name");
			$name = $names->item(0);
			$nameValue = $name->nodeValue;
			echo "<tr><td>".$nameValue."</td><td>".$value."</td></tr>";
		}
		$pos = false;
	}
}
echo "</table></body></html>"
?>